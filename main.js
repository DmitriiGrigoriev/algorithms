
function algorithm1(){
	var input = [4,7,2,5,4,5,6,7,9,6,5,2,7,4,1,2,3];
	var temp = [];
	var output;

	for(var i = 0; i < input.length; i++) {
		temp[i] = {
			array: [input[i]],
			pos: [i,input.length-1],
			arrLength: 1
		};
		for (j = i+1; j < input.length; j++) {
			if (temp[i].array.indexOf(input[j]) == -1) {
				temp[i].array.push(input[j]);
			} else {
				temp[i].pos[1] = j-1;
				temp[i].arrLength = temp[i].array.length;
				break;
			}
		}
	}

	return temp.reduce(function(prev, curr) {
			return (prev.arrLength > curr.arrLength) ? prev : curr;
		});
}

function algorithm2() {
	var input = "Lorem ipsum dolor sit amet consectetur adipisicing elit";

	return input.split(' ').reduceRight(function(prev, curr) {
		return prev + ' ' + curr;
	});
}

console.log('Algorithm 1: ', algorithm1());
console.log('Algorithm 2: ', algorithm2());